package com.lez19;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringHtmlLibriApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringHtmlLibriApplication.class, args);
	}

}
