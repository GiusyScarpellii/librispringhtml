CREATE DATABASE libri_spring;
USE libri_spring;

CREATE TABLE Categoria(
	id_categoria 	INT			 NOT NULL AUTO_INCREMENT,
    nome 			VARCHAR(250) NOT NULL,
    codice			VARCHAR(10)  NOT NULL UNIQUE,
    PRIMARY KEY(id_categoria)
);

-- Genere Libro One to Many
CREATE TABLE Libro (
	id_libro	INT			 NOT NULL UNIQUE AUTO_INCREMENT,
    ISBN		VARCHAR(20)  NOT NULL UNIQUE,
    titolo		VARCHAR(250) NOT NULL,
    autore		VARCHAR(250) NOT NULL,
    categoria	INT 		 NOT NULL,
    PRIMARY KEY (id_libro),
    FOREIGN KEY (categoria) REFERENCES Categoria (id_categoria) ON DELETE CASCADE
);