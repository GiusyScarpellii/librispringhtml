package com.lez19.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;

import lombok.NoArgsConstructor;

								
@AllArgsConstructor						
@NoArgsConstructor						
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "libri_categoria"})
@Entity
@Table
public class Categoria {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_categoria")
	private int 	id;
	@Column
	private String 	nome;
	@Column
	private String 	codice;
	@OneToMany(mappedBy="categoria_appartenenza", cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Libro> libri_categoria;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public List<Libro> getLibri_categoria() {
		return libri_categoria;
	}
	public void setLibri_categoria(List<Libro> libri_categoria) {
		this.libri_categoria = libri_categoria;
	}
	

	
}
