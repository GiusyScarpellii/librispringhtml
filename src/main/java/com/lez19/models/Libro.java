package com.lez19.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

								//Tutti i getter per ogni campo, utile per Hibernate
@AllArgsConstructor						//Genera il costruttore con tutti i parametri relativi agli attributi
@NoArgsConstructor						//Genera il costruttore senza parametri
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Table
public class Libro {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_libro")
	private int 	id;
	@Column(name="ISBN")
	private String 	isbn;
	@Column
	private String 	titolo;
	@Column
	private String 	autore;
	
	@ManyToOne
	@JoinColumn(name="categoria")
	private Categoria categoria_appartenenza;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getTitolo() {
		return titolo;
	}
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	public String getAutore() {
		return autore;
	}
	public void setAutore(String autore) {
		this.autore = autore;
	}
	public Categoria getCategoria_appartenenza() {
		return categoria_appartenenza;
	}
	public void setCategoria_appartenenza(Categoria categoria_appartenenza) {
		this.categoria_appartenenza = categoria_appartenenza;
	}
	
	
	

}
