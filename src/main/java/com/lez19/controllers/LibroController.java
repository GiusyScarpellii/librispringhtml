package com.lez19.controllers;


import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



import com.lez19.models.Libro;
import com.lez19.services.CategoriaService;
import com.lez19.services.LibroService;



@RestController
@RequestMapping("/libro")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class LibroController {
	
	
	@Autowired
	private LibroService service;
	@Autowired
	private CategoriaService serviceCat;
	
	@PostMapping("/insert")
	public Libro inserisci_libro(@RequestBody Libro objLib) {
		return service.insert(objLib);
	}
	
	@GetMapping("/cercaTutti")
	public List<Libro> lista_libri(){
		return service.findAll();
	}
	
	@GetMapping("/cercaPerId/{libro_id}")
	public Libro cercaPerId(@PathVariable int libro_id) {
		return service.findById(libro_id);
	}
	
	@DeleteMapping("/cancella/{libro_id}")
	public boolean eliminaLibro(@PathVariable int libro_id) {
		return service.delete(libro_id);
	}
	
	@PutMapping("/modifica/update")
	public boolean modificaLibro(@RequestBody Libro varLib) {
		return service.update(varLib);
	}

	@GetMapping("/assegnaCategoria/{libro_id}/{categoria_id}")
	public boolean assegnaCategoria(@PathVariable int libro_id, @PathVariable int categoria_id) {
		Libro temp = service.findById(libro_id);
		
		temp.setCategoria_appartenenza(serviceCat.findById(categoria_id));
		return service.update(temp);
	}
}
