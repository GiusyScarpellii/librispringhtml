package com.lez19.controllers;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lez19.models.Categoria;
import com.lez19.services.CategoriaService;

@RestController
@RequestMapping("/categoria")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CategoriaController {
	
	@Autowired
	private CategoriaService service;
	
	@PostMapping("/insert")
	public Categoria inserisci_categoria(@RequestBody Categoria objCat) {
		return service.insert(objCat);
	}
	
	@GetMapping("/cercaTutte")
	public List<Categoria> lista_categorie(){
		return service.findAll();
	}
	
	@GetMapping("/cercaPerId/{categoria_id}")
	public Categoria cercaPerId(@PathVariable int categoria_id) {
		return service.findById(categoria_id);
	}
	
	@DeleteMapping("/cancella/{categoria_id}")
	public boolean eliminaCategoria(@PathVariable int categoria_id) {
		return service.delete(categoria_id);
	}
	
	@PutMapping("/modifica/update")
	public boolean modificaCategoria(@RequestBody Categoria varCat) {
		return service.update(varCat);
	}
	

}
