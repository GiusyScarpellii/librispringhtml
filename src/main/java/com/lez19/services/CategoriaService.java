package com.lez19.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lez19.models.Categoria;

@Service
public class CategoriaService {
	
	@Autowired
	private EntityManager entMan;					
	
	private Session getSessione() {
		return entMan.unwrap(Session.class);
	}

	public Categoria insert(Categoria objCat) {
		
		Categoria temp = new Categoria();	
		temp.setNome(objCat.getNome());
		temp.setCodice(objCat.getCodice());
		
		
		Session sessione = getSessione();
		
		try {
			sessione.save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println("Errore: " + e.getMessage());
			
		}
	
		return null;
		
	}
	
	public Categoria findById(int varId) {
		Session sessione = getSessione();
		return(Categoria) sessione
							.createCriteria(Categoria.class)
							.add(Restrictions.eqOrIsNull("id", varId))
							.uniqueResult();
		
	}
	
	public List<Categoria> findAll(){
		Session sessione = getSessione();
		return sessione.createCriteria(Categoria.class).list();
	}
	
	@Transactional
	public boolean delete(int varId) {
		
		Session sessione = getSessione();
		
		try {
			Categoria temp = sessione.load(Categoria.class, varId);
			
			sessione.delete(temp);											
			sessione.flush();
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return false;
	}
	
	
	public boolean update(Categoria objCat) {
		
		Session sessione = getSessione();
		
		try {
			Categoria temp = sessione.load(Categoria.class, objCat.getId());
			
			if(temp != null) {
				temp.setNome(objCat.getNome());
				temp.setCodice(objCat.getCodice());
				
				sessione.update(temp);
				sessione.save(temp);
				sessione.persist(temp);
				sessione.flush();
				
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return false;
	}
}
