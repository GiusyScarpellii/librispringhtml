package com.lez19.services;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lez19.models.Categoria;
import com.lez19.models.Libro;

@Service
public class LibroService {
	
	@Autowired
	private EntityManager entMan;					
	
	private Session getSessione() {
		return entMan.unwrap(Session.class);
	}

	public Libro insert(Libro objLibro) {
		Libro temp = new Libro();
	
		temp.setIsbn(objLibro.getIsbn());
		temp.setTitolo(objLibro.getTitolo());
		temp.setAutore(objLibro.getAutore());
		
		Session sessione = getSessione();
		
		try {
			sessione.save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public Libro findById(int varId) {
		Session sessione = getSessione();
		return(Libro) sessione
							.createCriteria(Libro.class)
							.add(Restrictions.eqOrIsNull("id", varId))
							.uniqueResult();
	}
	
	public List<Libro> findAll() {
		Session session = getSessione();
		return session.createCriteria(Libro.class).list();
	}
	
	
	@Transactional
	public boolean delete (int varId) {
		Session sessione = getSessione();
		try {
			Libro temp = sessione.load(Libro.class, varId);
			
			sessione.delete(temp);
			sessione.flush();
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	@Transactional
	public boolean update(Libro objLibro) {
		Session sessione = getSessione();
		
		try {
			Libro temp = sessione.load(Libro.class, objLibro.getId());
			if(temp != null) {
				temp.setIsbn(objLibro.getIsbn());
				temp.setTitolo(objLibro.getTitolo());
				temp.setAutore(objLibro.getAutore());
				
				sessione.update(temp);
				sessione.save(temp);
				sessione.persist(temp);
				sessione.flush();
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
}

